#!/bin/bash

ALPINE_MAKE_VM_IMAGE="/tmp/alpine-make-vm-image"
ALPINE_MAKE_VM_IMAGE_URL="https://raw.githubusercontent.com/alpinelinux/alpine-make-vm-image/v0.13.0/alpine-make-vm-image"
ALPINE_MAKE_VM_IMAGE_SHA1="0fe2deca927bc91eb8ab32584574eee72a23d033"

VM_NAME="cryptsetup-alpine-edge-0"
POOL_FROM_CONFIG="$(grep -oP "storage_pool: +[']?\K.+[']?" ~/.config/lcitool/config.yml)"
NETWORK_FROM_CONFIG=$(grep -oP "network: +[']?\K.+[']?" ~/.config/lcitool/config.yml)
DISK_SIZE_FROM_CONFIG=$(grep -oP "disk_size: +[']?\K.+[']?" ~/.config/lcitool/config.yml)

POOL="${POOL_FROM_CONFIG:-gitlabci}"
NETWORK="${NETWORK_FROM_CONFIG:-gitlabci}"
DISK_SIZE="${DISK_SIZE_FROM_CONFIG:-15}"

if [[ $# -eq 1 ]]; then
	VM_NAME="$1"
fi

function cleanup() {
	rm -rf $INSTALL_SCRIPT $ALPINE_MAKE_VM_IMAGE
	rm -rf $INSTALL_SCRIPT
}

function fail() {
	echo "Failed to prepare AlpineLinux image: $1"
	cleanup
	virsh destroy $VM_NAME > /dev/null 2>&1
	virsh undefine $VM_NAME --remove-all-storage > /dev/null 2>&1
	exit 1
}

if [ $(id -u) != 0 ]; then
	echo "FAIL: You must be root to run this script."
	exit 0
fi

virsh domstate $VM_NAME > /dev/null 2>&1 && {
	read -p "The VM $VM_NAME already exists. Do you want to remove and reinstall it? (yes/no) " yn

	case $yn in
		yes ) ;;
		no ) echo Exiting...;
			exit 0;;
		* ) echo Invalid response;
			exit 1;;
	esac

	virsh destroy $VM_NAME
	virsh undefine --remove-all-storage $VM_NAME
}

DOM_XML=$(virt-install \
	--disk size=$DISK_SIZE,pool=$POOL,bus=virtio,format=qcow2 \
	--import \
	--os-variant unknown \
	--virt-type kvm \
	--arch x86_64 \
	--machine pc \
	--cpu host-passthrough \
	--vcpus 2 \
	--memory 4096 \
	--network network=$NETWORK,model=virtio \
	--graphics none \
	--console pty \
	--sound none \
	--rng device=/dev/urandom,model=virtio \
	--noautoconsole \
	--name "$VM_NAME" \
	--print-xml) || fail "Failed to create domain XML."

DOM_XML=$(echo "$DOM_XML" | sed '/<\/metadata>/i \
		<lcitool:host xmlns:lcitool="http://libvirt.org/schemas/lcitool/1.0">\
		  <lcitool:target>alpine-edge</lcitool:target>\
		</lcitool:host>')

virsh define --validate <(echo $DOM_XML) || fail "Failed to define domain from XML"
sleep 1
virsh domblklist $VM_NAME --details | awk '/disk/{print $4}'
ALPINE_IMAGE=$(virsh domblklist $VM_NAME --details | awk '/disk/{print $4}' | grep $VM_NAME.qcow2)

SSH_KEY=$(grep -oP " *ssh_key: *'\K[^']+" ~/.config/lcitool/config.yml 2> /dev/null)
if [[ -z "$SSH_KEY" ]]; then
	echo "No SSH key path found in ~/.config/lcitool/config.yml, using the default."
	SSH_KEY="~/.ssh/id_rsa.pub"
fi
SSH_KEY=${SSH_KEY/#\~/${HOME}}

echo "Injecting SSH key $SSH_KEY"

if [[ ! -e "$SSH_KEY" ]]; then
	fail "The SSH key $SSH_KEY does not exist."
fi

INSTALL_SCRIPT="$(mktemp /tmp/alpine-install-script.XXXXXX)"
if [[ $? != 0 ]]; then
	fail "Couldn't create temporary file for the install script."
fi

curl -s -o $ALPINE_MAKE_VM_IMAGE $ALPINE_MAKE_VM_IMAGE_URL
echo "$ALPINE_MAKE_VM_IMAGE_SHA1  $ALPINE_MAKE_VM_IMAGE" | \
	sha1sum --check > /dev/null 2>&1 || fail "The SHA1 for alpine-make-vm-image doesn't match."
chmod +x $ALPINE_MAKE_VM_IMAGE

# the script below runs on the alpine host, during installation
cat >> $INSTALL_SCRIPT <<INSTALL_END
#!/bin/sh

echo "Running Alpine customization..."

setup-keymap us us
setup-hostname alpine-runner
setup-devd udev
setup-ntp chrony
setup-timezone Europe/Prague
setup-sshd openssh

apk add \
	bash \
	bash-completion \
	cloud-init \
	coreutils \
	losetup \
	lsblk \
	mc \
	networkmanager-cli \
	openssh-server-pam \
	py3-netifaces \
	shadow \
	shadow-login \
	sudo \
	util-linux \
	vim

setup-cloud-init
apk add acpi
rc-update add acpid

apk add networkmanager
rc-update add networkmanager

cat << EOF > /etc/NetworkManager/NetworkManager.conf
[main]
dhcp=internal
plugins=ifupdown

[ifupdown]
managed=true
EOF

cat << EOF > /etc/NetworkManager/system-connections/eth0.nmconnection
[connection]
id=eth0
type=ethernet
autoconnect-priority=-999
interface-name=eth0

[ethernet]

[ipv4]
ignore-auto-dns=true
method=auto

[ipv6]
addr-gen-mode=stable-privacy
method=auto

[proxy]
EOF

chown root:root /etc/NetworkManager/system-connections/eth0.nmconnection
chmod 600 /etc/NetworkManager/system-connections/eth0.nmconnection

mkdir -p /root/.ssh/

apk add openssh-server-pam
rc-update add sshd
echo "UsePAM yes" >> /etc/ssh/sshd_config
echo "PermitRootLogin yes" >> /etc/ssh/sshd_config
echo "root:root" | chpasswd

mkdir /root/.ssh
echo "$(cat ${SSH_KEY})" >> /root/.ssh/authorized_keys

# cat > /etc/network/interfaces << EOF
# auto lo
# iface lo inet loopback
#
# auto eth0
# iface eth0 inet dhcp
#
# post-up /etc/network/if-post-up.d/*
# post-down /etc/network/if-post-down.d/*
# EOF
#
# rc-update add networking
INSTALL_END
chmod +x $INSTALL_SCRIPT

$ALPINE_MAKE_VM_IMAGE \
	--rootfs ext4 \
	--initfs-features "ata base ide scsi usb virtio ext3 cryptsetup" \
	--script-chroot \
	--serial-console \
	--image-format qcow2 \
	--kernel-flavor lts \
	--branch edge "$ALPINE_IMAGE" -- "$INSTALL_SCRIPT"

# echo "Done, starting the VM $VM_NAME"
# virsh start $VM_NAME --console

cleanup
exit 0
