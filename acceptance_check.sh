#!/bin/bash

DEREFERENCE_NULL="/tmp/dereference_null"
COREDUMPS_PATH="/var/coredumps"

function fail()
{
	echo -n "Acceptance test FAILED: "
	[ -n "$1" ] && echo "$1"
	echo "Backtrace:"
	while caller $frame; do ((frame++)); done
	exit 2
}

cat <<EOF | gcc -o $DEREFERENCE_NULL -x c -
int main()
{
	/* crash and generate coredump */
	*((int *) 0) = 0;

	return 0;
}
EOF

rm -rf ${COREDUMPS_PATH}/*
echo -n "INTENTIONALLY crashing a program to verify coredump is generated in correct location: "
$DEREFERENCE_NULL && fail "The program didn't crash"
sleep 1
COREDUMPS_COUNT=$(ls $COREDUMPS_PATH | wc -l)

[ $COREDUMPS_COUNT -ge 1 ] || fail "No coredump was generated"

SUDO_ASKPASS=/bin/false sudo -A whoami 2>&1 > /dev/null || fail "Passwordless sudo not working"

HOSTNAME=$(hostname)
if [[ "$HOSTNAME" =~ "rhel-9" ]] && [[ ! "$HOSTNAME" =~ "fips" ]]
then
	for script in /opt/{build-rpm.sh,run-annocheck.sh,run-csmock.sh}
	do
		[ -f $script ] || fail "Script $script not found."
	done
fi

if [[ "$HOSTNAME" =~ "rhel" ]] && [[ "$HOSTNAME" =~ "fips" ]]
then
	echo -n "Verifying FIPS mode: "
	sudo fips-mode-setup --is-enabled || fail "FIPS mode is not enabled on machine with 'fips' in hostname"
	[[ "$(sudo fips-mode-setup --check)" =~ "FIPS mode is enabled." ]] || fail "FIPS mode is not enabled on machine with 'fips' in hostname"
	echo "OK"
fi

sudo rm -rf cryptsetup
git clone --depth=1 https://gitlab.com/cryptsetup/cryptsetup.git
cd cryptsetup

./autogen.sh || fail
./configure || fail
make -j || fail
make -j -C tests check-programs || fail

if [[ "$HOSTNAME" =~ "rhel-9" ]] && [[ ! "$HOSTNAME" =~ "fips" ]]
then
	echo "Trying to build RPM"
	sudo git clean -xdf
	sudo git checkout -f

	sudo /opt/build-rpm.sh || fail "Failed to build RPM"
fi

echo "Acceptance test PASSED"
