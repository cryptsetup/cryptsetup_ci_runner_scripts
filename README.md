# generate ssh keys
ssh-keygen

# Install dependencies

## Fedora
```
curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.rpm.sh" | sudo bash
sudo dnf install -y ansible guestfs-tools libosinfo libvirt libvirt-devel libvirt-nss python3-cffi python3-devel qemu-kvm virt-install gitlab-runner sshpass
```

## Debian
```
curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo bash
sudo apt-get install ansible bridge-utils guestfs-tools libnss-libvirt libosinfo-1.0-0 libvirt-clients libvirt-daemon libvirt-daemon-system libvirt-dev pip python3-cffi python3-dev qemu-kvm virtinst gitlab-runner sshpass
```

## Install lcitool and libvirt-gci dependencies
It is recommended that you install the packages via system package manager, not directly via pip.

### Fedora
```
dnf install -y python3-ansible-runner python3-libvirt python3-paramiko python3-passlib python3-gobject python3-requests python3-tqdm python3-pyyaml 
```

### Debian
```
sudo apt-get install python3-ansible-runner python3-libvirt python3-paramiko python3-passlib python3-gi python3-requests python3-tqdm python3-yaml
```

### pip (not recommended)
```
pip install -r 'https://gitlab.com/libvirt/libvirt-ci/-/raw/master/vm-requirements.txt?inline=false'
pip install -r 'https://gitlab.com/libvirt/libvirt-ci/-/raw/master/requirements.txt?inline=false'
pip install -r 'https://gitlab.com/daniel.zatovic/libvirt-gitlab-executor/-/raw/master/requirements.txt?ref_type=heads&inline=false'
```

## Setup `libvirt-nss`
Edit `/etc/nsswitch.conf`, add `libvirt_guest` after files in hosts, it should look similar to:
```
hosts:          files libvirt_guest mdns4_minimal [NOTFOUND=return] dns myhostname
```

***NOTE: on systems, where nsswitch.conf is managed by authselect (e.g. Fedora), use the following command (otherwise your changes will get discarded):***
```
authselect enable-feature -b with-libvirt
```

# Install `lcitool`, `libvirt-gci` and set up the runner

## Install lcitool/libvirt-gitlab-executor
```
pip install --break-system-packages --upgrade --force-reinstall 'git+https://gitlab.com/libvirt/libvirt-ci.git@master'
pip install --break-system-packages --upgrade --force-reinstall 'git+https://gitlab.com/daniel.zatovic/libvirt-gitlab-executor.git'
```

## Pull the configuration

```
git clone https://gitlab.com/cryptsetup/cryptsetup_ci_runner_scripts.git
cd cryptsetup_ci_setup
```

## Setup `libvirt`

```
systemctl enable --now libvirtd

virsh net-define gitlabci-net.xml
virsh net-autostart gitlabci
virsh net-start gitlabci

# change this to directory, where VM images should be stored
IMAGES_PATH="/var/lib/libvirt/images/gitlabci"
VOLATILE_IMAGES_PATH="/var/lib/libvirt/images_volatile"

mkdir -p $IMAGES_PATH
mkdir -p $VOLATILE_IMAGES_PATH
echo "tmpfs  $VOLATILE_IMAGES_PATH  tmpfs  defaults,size=15G,x-gvfs-show  0  0" >> /etc/fstab
systemctl daemon-reload
mount $VOLATILE_IMAGES_PATH

virsh pool-define-as --name gitlabci --type dir --target $IMAGES_PATH
virsh pool-define-as --name gitlabci-volatile --type dir --target $VOLATILE_IMAGES_PATH
virsh pool-autostart gitlabci
virsh pool-start gitlabci
virsh pool-autostart gitlabci-volatile
virsh pool-start gitlabci-volatile
```

## Create `lcitool` and `libvirt-gci` config files

```
# set data dir to the root of cryptsetup-ci-setup repo
export DATA_DIR=$(pwd)
# change to "True", on old systems, where virt-install doesn't support cloud-init
export GENERATE_CLOUD_INIT_ISO="False"

mkdir -p ~/.config/{lcitool,libvirt-gci}
LCITOOL_CONFIG="config-lcitool.yml" envsubst < config-libvirt-gci.template.yml > ~/.config/libvirt-gci/config.yml
LCITOOL_CONFIG="config-lcitool-32bit.yml" envsubst < config-libvirt-gci.template.yml > ~/.config/libvirt-gci/config-32bit.yml

cp config-lcitool.yml ~/.config/lcitool/config.yml
cp config-lcitool-32bit.yml ~/.config/lcitool/config-32bit.yml
```

***Now edit the configs and change default root password (set it to something random)!***

# Register the runners
```
# register update runner
./register_updater_runner.sh <token from cryptsetup-ci-setup repo>
# register the actual runner
./register_runner.sh <token from cryptsetup upstream>
```

***increase the number of concurrent jobs in /etc/gitlab-runner/config.toml and /etc/gitlab-runner/config-gitlab-runner-updater.toml***

## Add authorized users (gitlab usernames) who can update/reinstall the images

```
echo mbroz >> ~/.config/gitlab-update-runner/authorized_users
echo oniko >> ~/.config/gitlab-update-runner/authorized_users
echo daniel.zatovic >> ~/.config/gitlab-update-runner/authorized_users
```

## Install all the targets

### Automatic way

Install/update using the Gitlab CI pipeline in `cryptsetup_ci_runner_scripts` repo.

### Manually

For every supported distro, run (for 32bit images add `--lcitool-config ~/.config/lcitool/config-32bit.yml`):
```
libvirt-gci install -m cryptsetup-<distro_name> -d <distro name>
libvirt-gci update -m cryptsetup-<distro_name>
```
