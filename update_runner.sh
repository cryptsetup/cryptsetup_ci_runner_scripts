#!/bin/bash

trap cleanup SIGTERM

ACTION=$1
SCRIPT=$2
STAGE=$3

function fail()
{
	[ -n "$1" ] && echo "$1"
	echo "FAILED backtrace:"
	while caller $frame; do ((frame++)); done
	exit 1
}

SERVICE_NAME="gitlab-runner.service"
# how long to wait for currently jobs to stop
GRACEFUL_TIMEOUT="2h"
LIBVIRT_GITLAB_EXECUTOR_GIT="https://gitlab.com/daniel.zatovic/libvirt-gitlab-executor.git"
LIBVIRT_CI_GIT="https://gitlab.com/libvirt/libvirt-ci.git"

SOURCE_PATH=$(dirname $(realpath ${BASH_SOURCE[0]}))
DATA_DIR="${DATA_DIR:-$SOURCE_PATH}"

CONFIG_DIR="${HOME}/.config/gitlab-update-runner"
AUTHORIZED_USERS_FILE="${CONFIG_DIR}/authorized_users"
export REFCOUNT_FILE="${CONFIG_DIR}/refcount.txt"
export REFCOUNT_LOCK="${CONFIG_DIR}/refcount.lock"
LOCK_FD=200

PIP="pip"
LIBVIRT_GCI="libvirt-gci"

LOCK_ACQUIRED=0
REFCOUNT_INCREMENTED=0

function acquire_lock() {
	[ "$LOCK_ACQUIRED" -eq 1 ] && fail "Cannot acquire lock multiple times!"

	[ -e "$REFCOUNT_LOCK" ] || touch "$REFCOUNT_LOCK"
	exec {LOCK_FD}>"$REFCOUNT_LOCK" || return 1

	echo -n "Acquiring refcount lock: "
	flock "$LOCK_FD" || fail "ERROR: flock() failed (returned $?)."
	export LOCK_ACQUIRED=1
	echo "OK"
}

function release_lock() {
	[ "$LOCK_ACQUIRED" -eq 0 ] && fail "Cannot release lock multiple times!"

	echo -n "Releasing refcount lock: "
	flock -u "$LOCK_FD" || fail "ERROR: releasing flock() failed (returned $?)."
	export LOCK_ACQUIRED=0
	echo "OK"
}

function get_service_pid()
{
	systemctl show --property MainPID "$SERVICE_NAME" | grep -oP '(?<=MainPID=)\d+'
}

function update_and_stop_service()
{
	SERVICE_PID=$(get_service_pid)

	sudo -i $PIP install --break-system-packages --force-reinstall --upgrade "git+${LIBVIRT_GITLAB_EXECUTOR_GIT}" || return 1
	sudo -i $PIP install --break-system-packages --force-reinstall --upgrade "git+${LIBVIRT_CI_GIT}" || return 1
	# git -C not supported on trantor
	(cd $DATA_DIR && git pull --rebase) || return 1

	if [ "$SERVICE_PID" != 0 ]
	then
		echo "[i] waiting for $SERVICE_NAME to stop"
		sudo -i systemctl kill -s SIGQUIT --kill-who=main "$SERVICE_NAME"
		sudo -i bash -c "timeout $GRACEFUL_TIMEOUT tail --pid=$SERVICE_PID -f /dev/null"
		sudo -i systemctl stop $SERVICE_NAME || return 1
	fi

	return 0
}

function start_service()
{
	echo "Update finished, starting $SERVICE_NAME"

	for i in $(seq 1 5)
	do
		systemctl start $SERVICE_NAME
		sleep $((i*2))
		SERVICE_PID=$(get_service_pid)
		if [ $SERVICE_PID == 0 ]
		then
			echo "Failed to start $SERVICE_NAME after update."
		else
			return 0
		fi
	done;

	return 1
}

function increment_refcount()
{
	[ "$LOCK_ACQUIRED" -eq 0 ] && fail "Cannot increment refcount when lock is not held!"
	[ "$REFCOUNT_INCREMENTED" -eq 1 ] && fail "Cannot increment refcount multiple times!"

	[ -s "$REFCOUNT_FILE" ] || {
		echo 0 > "$REFCOUNT_FILE"
	}
	REFCOUNT=$(cat "$REFCOUNT_FILE")
	[ "$REFCOUNT" -le 0 ] && REFCOUNT=0
	REFCOUNT=$((REFCOUNT + 1))

	echo "$REFCOUNT" > $REFCOUNT_FILE
	export REFCOUNT_INCREMENTED=1

	return 0
}

function decrement_refcount()
{
	[ "$LOCK_ACQUIRED" -eq 0 ] && fail "Cannot decrement refcount when lock is not held!"
	[ "$REFCOUNT_INCREMENTED" -eq 0 ] && fail "Cannot decrement refcount multiple times!"

	[ -s "$REFCOUNT_FILE" ] || {
		echo 0 > "$REFCOUNT_FILE"
	}
	REFCOUNT=$(cat "$REFCOUNT_FILE")
	[ "$REFCOUNT" -le 0 ] && REFCOUNT=1
	[ "$REFCOUNT" -eq 1 ] && start_service
	echo "$((REFCOUNT - 1))" > $REFCOUNT_FILE
	export REFCOUNT_INCREMENTED=0
	return 0
}

function prepare()
{
	acquire_lock
	increment_refcount
	REFCOUNT=$(cat "$REFCOUNT_FILE")

	echo "There are $REFCOUNT updater jobs running concurrently."

	[ "$REFCOUNT" -eq 1 ] && {
		update_and_stop_service || {
		decrement_refcount
			release_lock
			fail "Failed to prepare the environment."
		}
	}
	release_lock

	return 0
}

function finish()
{
	acquire_lock
	decrement_refcount
	release_lock
	return 0
}

function abort_update() {
	if virsh domstate "${CUSTOM_ENV_GITLAB_UPDATER_DISTRO}-update" >/dev/null 2>&1 \
	   || virsh domstate "${CUSTOM_ENV_GITLAB_UPDATER_DISTRO}-update-test" >/dev/null 2>&1
	then
		sudo -i $LIBVIRT_GCI cleanup -m "${CUSTOM_ENV_GITLAB_UPDATER_DISTRO}-update"
		sudo -i $LIBVIRT_GCI cleanup -m "${CUSTOM_ENV_GITLAB_UPDATER_DISTRO}-update-test"
	fi

	exit 0
}

function abort_reinstall() {
	REINSTALL_MACHINE_NAME="${CUSTOM_ENV_GITLAB_UPDATER_DISTRO}-0"

	if [ "$(virsh domstate "$REINSTALL_MACHINE_NAME" 2>/dev/null)" == "running" ]
	then
		sudo -i $LIBVIRT_GCI cleanup -m "$REINSTALL_MACHINE_NAME"
	fi
}

function update()
{
	DISTRO="$1"

	# increment the reference counter
	# if we are the first job to start running, disable gitlab-runner service and update tooling
	# afterward, decremen the reference counter and restart the service
	prepare

	SERVICE_PID=$(get_service_pid)
	[ "$SERVICE_PID" != 0 ] && fail "Cannot update, $SERVICE_NAME is still running."
	sudo -i $LIBVIRT_GCI --debug update -d $DISTRO || {
		finish
		fail "Update failed."
	}
	finish

	exit 0
}

function reinstall()
{
	IMAGE_NAME="$1"
	TARGET="$2"

	prepare

	SERVICE_PID=$(get_service_pid)
	[ "$SERVICE_PID" != 0 ] && fail "Cannot update, $SERVICE_NAME is still running."

	CONFIG_32BIT_FLAG=""
	if [ "${CUSTOM_ENV_GITLAB_UPDATER_USE_32BIT_CONFIG:-0}" -ne 0 ]
	then
		CONFIG_32BIT_FLAG="--lcitool-config ~/.config/lcitool/config-32bit.yml"
	fi

	if [ "$TARGET" == "alpine-edge" ]
	then
		for DOMAIN in $(virsh list --all | awk '{print $2}' | grep -E '^cryptsetup-alpine-edge-[0-9]+$')
		do
			virsh destroy $DOMAIN >/dev/null 2>&1
			virsh undefine --remove-all-storage $DOMAIN >/dev/null 2>&1
		done

		yes yes | "${DATA_DIR}/prepare_alpine.sh" "${IMAGE_NAME}-0" || {
			finish
			fail "Image install failed."
		}
	else
		sudo -i $LIBVIRT_GCI --debug install --force -m "$IMAGE_NAME" -d "$TARGET" $CONFIG_32BIT_FLAG || {
			finish
			fail "Image install failed."
		}
	fi

	finish
	exit 0
}

function revert()
{
	DISTRO="$1"

	prepare

	SERVICE_PID=$(get_service_pid)
	[ "$SERVICE_PID" != 0 ] && fail "Cannot update, $SERVICE_NAME is still running."

	sudo -i $LIBVIRT_GCI --debug revert -d "$DISTRO" || {
		finish
		fail "Update failed."
	}

	finish
	exit 0
}

function cleanup_current_action()
{
	case $CUSTOM_ENV_GITLAB_UPDATER_ACTION in
		update )	abort_update; ;;
		reinstall )	abort_reinstall; ;;
		* )		echo "Invalid action '$CUSTOM_ENV_GITLAB_UPDATER_ACTION'"; exit 1;;
	esac
}

function cleanup()
{
	echo "Job terminated, cleaning up.."
	cleanup_current_action

	[ "$REFCOUNT_INCREMENTED" -eq 1 ] && {
		[ "$LOCK_ACQUIRED" -eq 0 ] && {
			acquire_lock
		}
		decrement_refcount
		release_lock
	}

	exit 0
}

mkdir -p "$CONFIG_DIR"
[ ! -e "$AUTHORIZED_USERS_FILE" ] && touch "$AUTHORIZED_USERS_FILE"

grep -Fxq "$CUSTOM_ENV_GITLAB_USER_LOGIN" "$AUTHORIZED_USERS_FILE" || fail "Permission denied! You do not have permissions to run the pipeline."

if [ "$ACTION" == "cleanup" ]
then
	cleanup_current_action
fi

if [ "$ACTION" != "run" ] || [ "$STAGE" != "build_script" ]
then
	exit 0
fi

case $CUSTOM_ENV_GITLAB_UPDATER_ACTION in
	update )	update $CUSTOM_ENV_GITLAB_UPDATER_DISTRO; ;;
	reinstall )	reinstall $CUSTOM_ENV_GITLAB_UPDATER_IMAGE_NAME $CUSTOM_ENV_GITLAB_UPDATER_TARGET; ;;
	revert )	revert $CUSTOM_ENV_GITLAB_UPDATER_DISTRO; ;;
	* )		echo "Invalid action '$CUSTOM_ENV_GITLAB_UPDATER_ACTION'"; exit 1;;
esac
