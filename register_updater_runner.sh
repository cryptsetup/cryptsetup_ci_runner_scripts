#!/bin/bash

if [[ ! $# -eq 1 ]]; then
	echo "Usage: $0 <Gitlab token (mandatory)>"
	echo "You can override runner name via RUNNER_NAME environment variable (otherwise hostname is used)."
	exit 1
fi

function fail()
{
	[ -n "$1" ] && echo "$1"
	echo "FAILED backtrace:"
	while caller $frame; do ((frame++)); done
	exit 2
}


# maximum timeout for job (in hours)
GITLAB_URL="${GITLAB_URL:-https://gitlab.com}"

UPDATER_SERVICE_NAME_FULL="gitlab-runner-updater.service"
UPDATER_SERVICE_NAME="${UPDATER_SERVICE_NAME_FULL%.*}"
UPDATER_SERVICE_CONFIG="/etc/gitlab-runner/config-${UPDATER_SERVICE_NAME%.*}.toml"
UPDATER_SERVICE_USERNAME="gitlab-runner"
UPDATER_SERVICE_WORKING_DIRECTORY="/home/gitlab-runner"

SOURCE_PATH=$(dirname $(realpath ${BASH_SOURCE[0]}))
DATA_DIR="${DATA_DIR:-$SOURCE_PATH}"
# this variable is picked up by gitlab-runner
export RUNNER_NAME="${RUNNER_NAME:-$(hostname)}-updater"

if ! systemctl list-units --full -all | grep -Fq $UPDATER_SERVICE_NAME_FULL
then
	echo "Updater gitlab-runner service '$UPDATER_SERVICE_NAME_FULL' does not exist, creating.."

	gitlab-runner install \
		--service "$UPDATER_SERVICE_NAME" \
		--config "$UPDATER_SERVICE_CONFIG" \
		--user "$UPDATER_SERVICE_USERNAME" \
		--working-directory "$UPDATER_SERVICE_WORKING_DIRECTORY" \
		--syslog || fail "Failed to install $UPDATER_SERVICE_NAME_FULL service"

	systemctl start $UPDATER_SERVICE_NAME_FULL || fail "Failed to start $UPDATER_SERVICE_NAME_FULL"

	echo "Created service '$UPDATER_SERVICE_NAME_FULL' successfully."
fi

GITLAB_TOKEN="$1"

echo "Trying to unregister the runner first..."
gitlab-runner unregister \
	--config $UPDATER_SERVICE_CONFIG \
	--url $GITLAB_URL \
	--name $RUNNER_NAME > /dev/null 2>&1

echo "Registering the runner..."
gitlab-runner register \
	--config $UPDATER_SERVICE_CONFIG \
	--builds-dir /home/gitlab-runner/builds \
	--cache-dir /home/gitlab-runner/cache \
	--custom-run-args run \
	--custom-run-exec $DATA_DIR/update_runner.sh \
	--custom-cleanup-args cleanup \
	--custom-cleanup-exec $DATA_DIR/update_runner.sh \
	--executor custom \
	--non-interactive \
	--registration-token $GITLAB_TOKEN \
	--custom-graceful-kill-timeout 120 \
	--url $GITLAB_URL || fail "Failed to register the runner to GitLab (is the token wrong?)"
