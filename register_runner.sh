#!/bin/bash

if [[ $# -eq 0 ]]; then
	echo "Usage: $0 <Gitlab token (mandatory)>"
	echo "You can override runner name via RUNNER_NAME environment variable (otherwise hostname is used)."
	exit 1
fi

# this variable is picked up by gitlab-runner
export RUNNER_NAME="${RUNNER_NAME:-$(hostname)}"

GITLAB_TOKEN="$1"
shift
GITLAB_URL="${GITLAB_URL:-https://gitlab.com}"

echo "Trying to unregister the runner first..."
gitlab-runner unregister \
	--url $GITLAB_URL \
	--name $RUNNER_NAME > /dev/null 2>&1

echo "Registering the runner..."
gitlab-runner register \
	--builds-dir /home/gitlab-runner/builds \
	--cache-dir /home/gitlab-runner/cache \
	--custom-cleanup-args cleanup \
	--custom-cleanup-exec /usr/local/bin/libvirt-gci \
	--custom-prepare-args prepare \
	--custom-prepare-exec /usr/local/bin/libvirt-gci \
	--custom-run-args run \
	--custom-run-args --script \
	--custom-run-exec /usr/local/bin/libvirt-gci \
	--executor custom \
	--non-interactive \
	--token $GITLAB_TOKEN \
	--url $GITLAB_URL
